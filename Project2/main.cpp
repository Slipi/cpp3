#include <iostream>
#include <vector>
class Vehicle {
public:
	virtual void print() = 0;
	virtual float GetPower() = 0;
	friend std::ostream& operator<<(std::ostream& out, const Vehicle& v);
	virtual ~Vehicle() {};
};
std::ostream& operator<<(std::ostream& out, Vehicle& v) {
	v.print();
	return out;
}


class WaterVehicle : public Vehicle {
public:
	WaterVehicle(float s) : sag(s) {};
	void print() override {
		std::cout << "WaterVehicle Sag: " << sag << std::endl;
	}
	float GetPower() override {
		return 0;
	}
	float getSag() { return sag; }
	virtual ~WaterVehicle() {};
private:
	float sag;
};
class RoadVehicle : public Vehicle {
public:
	RoadVehicle(float height) : ride_height(height) {};
	float GetPower() override {
		return 0;
	}
	float getHeight() { return ride_height; }
	virtual ~RoadVehicle() {};
private:
	float ride_height;
};



class Wheel {
public:
	Wheel(float d) : diameter(d) {};
	float getD() { return diameter; }
private:
	float diameter;
};
class Engine {
public:
	Engine(float e) : power(e) {};
	float getPower() {return power;}
private:
	float power;
};


class Bicycle : public RoadVehicle {
public:
	Bicycle(Wheel W1, Wheel W2, float height) :
		w1(W1), w2(W2), RoadVehicle(height) {}
	void print() override {
		std::cout << "Bicycle Wheels: " << w1.getD() << " " << w2.getD()<< " Ride height: " 
			<< RoadVehicle::getHeight() << std::endl;
	}
	float GetPower() override{
		return 0;
	}
	virtual ~Bicycle() {};
private:
	Wheel w1, w2;
};
class Car : public RoadVehicle {
public:
	Car(Engine E, Wheel W1, Wheel W2, Wheel W3, Wheel W4, float height): 
		e(E), w1(W1), w2(W2), w3(W3), w4(W4), RoadVehicle(height) {}
	void print() override {
		std::cout << "Car Engine: " << e.getPower() << " Wheels: "
			<< w1.getD() << " " << w2.getD() << " " << w3.getD() << " " << w4.getD()
			<< " Ride height: " << RoadVehicle::getHeight() << std::endl;
	}
	float GetPower() override {
		return e.getPower();;
	}
	virtual ~Car() {};
private:
	Wheel w1, w2, w3, w4;
	Engine e;
};

class Point {
public:
	Point(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}
	friend std::ostream& operator<<(std::ostream& out, const Point& p);
private:
	float x, y, z;
};
std::ostream& operator<<(std::ostream& out, const Point& p) {
	out << p.x << " " << p.y << " " << p.z;
	return out;
}

class Circle : public Vehicle {
public:
	Circle(Point p, float d) : center(p), deameter(d) {}
	void print() override {
		std::cout << "Circle Center " << center << " Deameter: " << deameter << std::endl;
	}
	virtual ~Circle() {};
	float GetPower() override {
		return 0;
	}
private:
	Point center;
	float deameter;
};
void foo1() {
	Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);

	std::cout << c;

	Bicycle t(Wheel(20), Wheel(20), 300);

	std::cout << t;
}

float getHighestPower(std::vector<Vehicle*> v) {
	float maxPower = 0;
	for (int i = 0; i < v.size(); i++) {
		if (v[i]->GetPower() > maxPower) maxPower = v[i]->GetPower();
	}
	return maxPower;
}
void foo2() {
	std::vector<Vehicle*> v;

	v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));

	v.push_back(new Circle(Point(1, 2, 3), 7));

	v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));

	v.push_back(new WaterVehicle(5000));

	for (int i = 0; i < v.size(); i++)
		std::cout << *v[i];

	std::cout << "The highest power is " << getHighestPower(v) << '\n';
	
	for (int i = 0; i < v.size(); i++)
		delete v[i];
}
int main() {
	foo1();
	std::cout << "\n\n\n";
	foo2();

	return 0;
}